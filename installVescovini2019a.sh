# One click install script for Dune, DuMuX and Vescovini2019a
DUNE_VERSION=2.6

# check some prerequistes
for PRGRM in git cmake gcc g++ wget pkg-config; do
    if ! [ -x "$(command -v $PRGRM)" ]; then
        echo "Error: $PRGRM is not installed." >&2
        exit 1
    fi
done

# check some library prerequistes
for LIBRARY in libumfpack libsuitesparse; do
    if ! [ "$(/sbin/ldconfig -p | grep $LIBRARY)" ]; then
        echo "Error: $LIBRARY is not installed." >&2
        exit 1
    fi
done

# check the version of the compiler
currentver="$(gcc -dumpversion)"
requiredver="4.9.0"
if [ "$(printf '%s\n' "$requiredver" "$currentver" | sort -V | head -n1)" != "$requiredver" ]; then
    echo "gcc greater than or equal to $requiredver is required!" >&2
    exit 1
fi

echo "*********************************************************************************************"
echo "(1/2) Cloning repositories. This may take a while. Make sure to be connected to the internet."
echo "*********************************************************************************************"
# the core modules
for MOD in common geometry grid localfunctions istl; do
    if [ ! -d "dune-$MOD" ]; then
        git clone -b releases/$DUNE_VERSION https://gitlab.dune-project.org/core/dune-$MOD.git
    else
        echo "Skip cloning dune-$MOD because the folder already exists."
    fi
    cd dune-$MOD
    git checkout releases/$DUNE_VERSION
    cd ..
done

# dune-subgrid
if [ ! -d "dune-subgrid" ]; then
    git clone -b releases/$DUNE_VERSION-1 https://git.imp.fu-berlin.de/agnumpde/dune-subgrid.git
else
    echo "Skip cloning dune-subgrid because the folder already exists."
fi
cd dune-subgrid
git checkout releases/$DUNE_VERSION-1
cd ..

# dumux
if [ ! -d "dumux" ]; then
    git clone -b master https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
else
    echo "Skip cloning dumux because the folder already exists."
fi
cd dumux
	git reset --hard 7a138b725c131528f334457adc35aaa3fa701a6f
cd ..

# vescovini2019a
if [ ! -d "vescovini2019a" ]; then
    git clone -b master https://git.iws.uni-stuttgart.de/dumux-pub/vescovini2019a.git
else
    echo "Skip cloning vescovini2019a because the folder already exists."
fi

if [ $? -ne 0 ]; then
    echo "*********************************************************************************************"
    echo "Failed to clone the repositories."
    echo "*********************************************************************************************"
    exit $?
fi

echo "*********************************************************************************************"
echo "(2/2) Configure dune and dumux modules. Build the dune libaries. This may take several minutes."
echo "*********************************************************************************************"

# run build
./dune-common/bin/dunecontrol --opts=./dumux/cmake.opts all
#
if [ $? -ne 0 ]; then
    echo "*********************************************************************************************"
    echo "Failed to configure the libaries."
    echo "*********************************************************************************************"
    exit $?
fi

# echo result
echo "*********************************************************************************************"
echo "Succesfully configured and built dune and dumux."
echo "*********************************************************************************************"
