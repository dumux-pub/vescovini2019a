add_executable(rough_channel_navierstokes EXCLUDE_FROM_ALL main.cc)
target_compile_definitions(rough_channel_navierstokes PUBLIC "UPWINDSCHEMEORDER=1")

add_executable(rough_channel_navierstokes_tvd EXCLUDE_FROM_ALL main.cc)
target_compile_definitions(rough_channel_navierstokes_tvd PUBLIC "UPWINDSCHEMEORDER=2")

dune_symlink_to_source_files(FILES rough_channel_navierstokes.input
                                   rough_channel_navierstokes_low_re.input
                                   rough_channel_navierstokes_high.input)
