add_executable(rough_channel_turbulent_kepsilon EXCLUDE_FROM_ALL main.cc)
target_compile_definitions(rough_channel_turbulent_kepsilon PUBLIC "TYPETAG=RCKEpsilon")

add_executable(rough_channel_turbulent_kepsilon_tvd EXCLUDE_FROM_ALL main.cc)
target_compile_definitions(rough_channel_turbulent_kepsilon_tvd PUBLIC "TYPETAG=RCKEpsilon" "UPWINDSCHEMEORDER=2")

add_executable(rough_channel_turbulent_komega EXCLUDE_FROM_ALL main.cc)
target_compile_definitions(rough_channel_turbulent_komega PUBLIC "TYPETAG=RCKOmega")

add_executable(rough_channel_turbulent_komega_tvd EXCLUDE_FROM_ALL main.cc)
target_compile_definitions(rough_channel_turbulent_komega_tvd PUBLIC "TYPETAG=RCKOmega" "UPWINDSCHEMEORDER=2")

add_executable(rough_channel_turbulent_lowrekepsilon EXCLUDE_FROM_ALL main.cc)
target_compile_definitions(rough_channel_turbulent_lowrekepsilon PUBLIC "TYPETAG=RCLowReKEpsilon")

add_executable(rough_channel_turbulent_lowrekepsilon_tvd EXCLUDE_FROM_ALL main.cc)
target_compile_definitions(rough_channel_turbulent_lowrekepsilon_tvd PUBLIC "TYPETAG=RCLowReKEpsilon" "UPWINDSCHEMEORDER=2")

add_executable(rough_channel_turbulent_zeroeq EXCLUDE_FROM_ALL main.cc)
target_compile_definitions(rough_channel_turbulent_zeroeq PUBLIC "TYPETAG=RCZeroEq")

add_executable(rough_channel_turbulent_zeroeq_tvd EXCLUDE_FROM_ALL main.cc)
target_compile_definitions(rough_channel_turbulent_zeroeq_tvd PUBLIC "TYPETAG=RCZeroEq" "UPWINDSCHEMEORDER=2")

dune_symlink_to_source_files(FILES rough_channel_turbulent.input
                                   rough_channel_turbulent_high.input)
