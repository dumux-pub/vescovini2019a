add_executable(kovasznay_navierstokes EXCLUDE_FROM_ALL main.cc)
target_compile_definitions(kovasznay_navierstokes PUBLIC "UPWINDSCHEMEORDER=1")

add_executable(kovasznay_navierstokes_tvd EXCLUDE_FROM_ALL main.cc)
target_compile_definitions(kovasznay_navierstokes_tvd PUBLIC "UPWINDSCHEMEORDER=2")

dune_symlink_to_source_files(FILES params.input)
