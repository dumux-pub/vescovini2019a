// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
 /*!
  * \file
  * \brief The free-flow sub problem
  */
#ifndef DUMUX_RANS1P_SUBPROBLEM_HH
#define DUMUX_RANS1P_SUBPROBLEM_HH

#ifndef UPWINDSCHEMEORDER
#define UPWINDSCHEMEORDER 1
#endif

#include <dune/grid/yaspgrid.hh>
#include <dune/subgrid/subgrid.hh>

#include <dumux/material/fluidsystems/1pgas.hh>
#include <dumux/material/components/constant.hh>

#include <dumux/discretization/staggered/freeflow/properties.hh>
#include <dumux/freeflow/turbulenceproperties.hh>
#include <dumux/freeflow/rans/problem.hh>
#include <dumux/freeflow/turbulencemodel.hh>
#include <dumux/freeflow/rans/twoeq/komega/model.hh>
#include <dumux/freeflow/rans/twoeq/komega/problem.hh>

namespace Dumux {

template <class TypeTag>
class FreeFlowSubProblem;

namespace Properties {

// Create the new TypeTag
namespace TTag {
struct RANSModel { using InheritsFrom = std::tuple<StaggeredFreeFlowModel>; };
struct StokesOneP { using InheritsFrom = std::tuple<RANSModel, KOmega>; };
} // end namespace TTag

// The fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::StokesOneP>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
public:
    using type = FluidSystems::OnePGas<Scalar, Components::Constant<1, Scalar> >;
};

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::StokesOneP>
{
private:
    static constexpr auto dim = 2;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
public:
    using HostGrid = Dune::YaspGrid<dim, Dune::TensorProductCoordinates<Scalar, dim> >;
    using type = Dune::SubGrid<dim, HostGrid>;
};

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::StokesOneP> { using type = Dumux::FreeFlowSubProblem<TypeTag>; };

template<class TypeTag>
struct EnableFVGridGeometryCache<TypeTag, TTag::StokesOneP> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::StokesOneP> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::StokesOneP> { static constexpr bool value = true; };
template<class TypeTag>
struct UpwindSchemeOrder<TypeTag, TTag::StokesOneP> { static constexpr int value = UPWINDSCHEMEORDER; };
}

/*!
 * \brief The free-flow sub problem
 */
template <class TypeTag>
class FreeFlowSubProblem : public RANSProblem<TypeTag>
{
    using ParentType = RANSProblem<TypeTag>;

    using GridView = GetPropType<TypeTag, Properties::GridView>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;

    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;

    using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using Element = typename GridView::template Codim<0>::Entity;

    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;

    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;
    using TimeLoopPtr = std::shared_ptr<TimeLoop<Scalar>>;

    static constexpr auto dimWorld = FVGridGeometry::GridView::dimensionworld;

public:
    FreeFlowSubProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry, std::shared_ptr<CouplingManager> couplingManager)
    : ParentType(fvGridGeometry, "RANS"), eps_(1e-7), couplingManager_(couplingManager)
    {
        inletVelocity_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.InletVelocity");
        outletPressure_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.OutletPressure", 1.1e5);
        Scalar kinematicViscosity = getParam<Scalar>("Component.GasKinematicViscosity");

        Dumux::TurbulenceProperties<Scalar, dimWorld, true> turbulenceProperties;
        turbulentKineticEnergy_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.InletTurbulentKineticEnergy",
                                                            turbulenceProperties.turbulentKineticEnergy(inletVelocity_, height_(), kinematicViscosity));
        dissipation_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.InletDissipationRate",
                                                 turbulenceProperties.dissipationRate(inletVelocity_, height_(), kinematicViscosity));

        darcyStart_ = getParamFromGroup<Scalar>(this->paramGroup(), "Grid.DarcyStart");
        darcyEnd_ = getParamFromGroup<Scalar>(this->paramGroup(), "Grid.DarcyEnd");

        stepHeight_ = getParamFromGroup<Scalar>(this->paramGroup(), "Grid.StepHeight");
        Scalar stepDistance = getParamFromGroup<Scalar>(this->paramGroup(), "Grid.StepDistance");
        Scalar stepLength = getParamFromGroup<Scalar>(this->paramGroup(), "Grid.StepLength");

        stepCoords_[0][0] = getParamFromGroup<Scalar>(this->paramGroup(), "Grid.StepBegin");
        stepCoords_[0][1] = stepCoords_[0][0] + stepLength;
        stepCoords_[1][0] = stepCoords_[0][1] + stepDistance;
        stepCoords_[1][1] = stepCoords_[1][0] + stepLength;

        std::cout << std::endl;
    }

    /*!
     * \name Boundary Locations
     */
    // \{

    bool isOnWallAtPos(const GlobalPosition& globalPos) const
    {
        return onLowerBoundary_(globalPos);
    }
    // \}

   /*!
     * \name Problem parameters
     */
    // \{

   /*!
     * \brief Return the temperature within the domain in [K].
     */
    Scalar temperature() const
    { return 298.15; }

   /*!
     * \brief Return the sources within the domain.
     *
     * \param globalPos The global position
     */
    NumEqVector sourceAtPos(const GlobalPosition &globalPos) const
    { return NumEqVector(0.0); }

    // \}
   /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param element The finite element
     * \param scvf The sub control volume face
     */
    BoundaryTypes boundaryTypes(const Element& element,
                                const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes bTypes;

        const auto& globalPos = scvf.center();

        if (onRightBoundary_(globalPos))
        {
            bTypes.setDirichlet(Indices::pressureIdx);
            bTypes.setOutflow(Indices::turbulentKineticEnergyEqIdx);
            bTypes.setOutflow(Indices::dissipationEqIdx);
        }
        else if (onUpperBoundary_(globalPos))
        {
            bTypes.setAllSymmetry();
        }
        else
        {
            bTypes.setDirichlet(Indices::velocityXIdx);
            bTypes.setDirichlet(Indices::velocityYIdx);
            bTypes.setDirichlet(Indices::turbulentKineticEnergyIdx);
            bTypes.setDirichlet(Indices::dissipationIdx);
        }

        if (isOnCouplingWall_(scvf))
        {
            bTypes.setCouplingNeumann(Indices::conti0EqIdx);
            bTypes.setCouplingNeumann(scvf.directionIndex());
            bTypes.setBJS(1 - scvf.directionIndex());
        }

        return bTypes;
    }

    /*!
    * \brief Returns whether a fixed Dirichlet value shall be used at a given cell.
    *
    * \param element The finite element
    * \param fvGeometry The finite-volume geometry
    * \param scv The sub control volume
    */
    template<class Element, class FVElementGeometry, class SubControlVolume>
    bool isDirichletCell(const Element& element,
                         const FVElementGeometry& fvGeometry,
                         const SubControlVolume& scv,
                         int pvIdx) const
    {
        // set a fixed dissipation (omega) for all cells at the wall
        for (const auto& scvf : scvfs(fvGeometry))
            if (isOnWallAtPos(scvf.center()) && pvIdx == Indices::dissipationIdx)
                return true;

        return false;
    }

     /*!
      * \brief Evaluate the boundary conditions for a dirichlet values at the boundary.
      *
      * \param element The finite element
      * \param scvf the sub control volume face
      * \note used for cell-centered discretization schemes
      */
    PrimaryVariables dirichlet(const Element &element, const SubControlVolumeFace &scvf) const
    {
        return initialAtPos(scvf.ipGlobal());
    }

     /*!
      * \brief Evaluate the boundary conditions for fixed values at cell centers
      *
      * \param element The finite element
      * \param scv the sub control volume
      * \note used for cell-centered discretization schemes
      */
    PrimaryVariables dirichlet(const Element &element, const SubControlVolume &scv) const
    {
        const auto globalPos = scv.center();
        PrimaryVariables values(initialAtPos(globalPos));
        unsigned int elementIdx = this->fvGridGeometry().elementMapper().index(element);
        const auto wallDistance = ParentType::wallDistance_[elementIdx];
        values[Indices::dissipationEqIdx] = 6.0 * ParentType::kinematicViscosity_[elementIdx]
                                            / (ParentType::betaOmega() * wallDistance * wallDistance);
        return values;
    }

    /*!
     * \brief Evaluate the boundary conditions for a Neumann control volume.
     *
     * \param element The element for which the Neumann boundary condition is set
     * \param fvGeomentry The fvGeometry
     * \param elemVolVars The element volume variables
     * \param elemFaceVars The element face variables
     * \param scvf The boundary sub control volume face
     */
    template<class ElementVolumeVariables, class ElementFaceVariables>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFaceVariables& elemFaceVars,
                        const SubControlVolumeFace& scvf) const
    {
        PrimaryVariables values(0.0);
        if(couplingManager().isCoupledEntity(CouplingManager::stokesIdx, scvf))
        {
            values[Indices::conti0EqIdx] = couplingManager().couplingData().massCouplingCondition(element, fvGeometry, elemVolVars, elemFaceVars, scvf);
            values[scvf.directionIndex()] = couplingManager().couplingData().momentumCouplingCondition(element, fvGeometry, elemVolVars, elemFaceVars, scvf);
        }
        return values;
    }

    // \}

    /*!
     * \brief Set the coupling manager
     */
    void setCouplingManager(std::shared_ptr<CouplingManager> cm)
    { couplingManager_ = cm; }

    /*!
     * \brief Get the coupling manager
     */
    const CouplingManager& couplingManager() const
    { return *couplingManager_; }

   /*!
     * \name Volume terms
     */
    // \{

   /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param globalPos The global position
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values(0.0);

        values[Indices::pressureIdx] = outletPressure_;

        if (globalPos[1] > stepHeight_ + eps_)
           values[Indices::velocityXIdx] = inletVelocity_;

        values[Indices::turbulentKineticEnergyIdx] = turbulentKineticEnergy_;
        values[Indices::dissipationIdx] = dissipation_;

        if (isOnWallAtPos(globalPos))
        {
            values[Indices::velocityXIdx] = 0.0;
            values[Indices::turbulentKineticEnergyIdx] = 0.0;
        }

        return values;
    }

    void setTimeLoop(TimeLoopPtr timeLoop)
    { timeLoop_ = timeLoop; }

    /*!
     * \brief Returns the intrinsic permeability of required as input parameter for the Beavers-Joseph-Saffman boundary condition
     */
    Scalar permeability(const Element& element, const SubControlVolumeFace& scvf) const
    {
        return couplingManager().couplingData().darcyPermeability(element, scvf);
    }

    /*!
     * \brief Returns the alpha value required as input parameter for the Beavers-Joseph-Saffman boundary condition
     */
    Scalar alphaBJ(const SubControlVolumeFace& scvf) const
    {
        return couplingManager().problem(CouplingManager::darcyIdx).spatialParams().beaversJosephCoeffAtPos(scvf.center());
    }

    // \}

private:
    bool onRightBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] > this->fvGridGeometry().bBoxMax()[0] - eps_; }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    {
        if (globalPos[1] > this->fvGridGeometry().bBoxMin()[1] + stepHeight_ + eps_)
            return false;

        if (globalPos[1] > this->fvGridGeometry().bBoxMin()[1] + eps_ && isInCavities_(globalPos[0]))
            return false;
        return true;
    }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] > this->fvGridGeometry().bBoxMax()[1] - eps_; }

    bool isInCavities_(const Scalar& x) const
    {
        if ((x > stepCoords_[0][0] && x < stepCoords_[0][1]) || (x > stepCoords_[1][0] && x < stepCoords_[1][1]))
            return true;
        else
            return false;
    }

    bool isOnCouplingWall_(const SubControlVolumeFace& scvf) const
    {
        return couplingManager().isCoupledEntity(CouplingManager::stokesIdx, scvf);
    }

    // the height of the free-flow domain
    const Scalar height_() const
    { return this->fvGridGeometry().bBoxMax()[1] - this->fvGridGeometry().bBoxMin()[1]; }

    Scalar eps_;

    Scalar inletVelocity_;
    Scalar outletPressure_;
    Scalar turbulentKineticEnergy_;
    Scalar dissipation_;
    Scalar darcyEnd_;
    Scalar darcyStart_;
    Scalar stepHeight_;
    std::array<std::array<Scalar, 2>, 2> stepCoords_;

    TimeLoopPtr timeLoop_;

    std::shared_ptr<CouplingManager> couplingManager_;

};

} // end namespace Dumux

#endif // DUMUX_RANS1P_SUBPROBLEM_HH
