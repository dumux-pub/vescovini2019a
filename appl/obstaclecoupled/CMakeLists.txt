add_executable(obstacle_coupled EXCLUDE_FROM_ALL main.cc)

add_executable(obstacle_coupled_tvd EXCLUDE_FROM_ALL main.cc)
target_compile_definitions(obstacle_coupled_tvd PUBLIC "UPWINDSCHEMEORDER=2")

add_executable(obstacle_coupled_forchheimer EXCLUDE_FROM_ALL main.cc)
target_compile_definitions(obstacle_coupled_forchheimer PUBLIC "FORCHHEIMER=1")

add_executable(obstacle_coupled_forchheimer_tvd EXCLUDE_FROM_ALL main.cc)
target_compile_definitions(obstacle_coupled_forchheimer_tvd PUBLIC "UPWINDSCHEMEORDER=2" "FORCHHEIMER=1")

dune_symlink_to_source_files(FILES params.input)
