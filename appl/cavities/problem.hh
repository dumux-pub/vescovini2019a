// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Turbulent isothermal flow test with cavities used to see how they influence each other.
 *        The k-omega model is used for the RANS equations.
 */
#ifndef DUMUX_CAVITIES_PROBLEM_HH
#define DUMUX_CAVITIES_PROBLEM_HH

#ifndef UPWINDSCHEMEORDER
#define UPWINDSCHEMEORDER 1
#endif

#include <dune/grid/yaspgrid.hh>
#include <dune/subgrid/subgrid.hh>

#include <dumux/discretization/staggered/freeflow/properties.hh>
#include <dumux/freeflow/turbulenceproperties.hh>
#include <dumux/freeflow/rans/problem.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/components/constant.hh>

#include <dumux/freeflow/turbulencemodel.hh>
#include <dumux/freeflow/rans/twoeq/komega/model.hh>
#include <dumux/freeflow/rans/twoeq/komega/problem.hh>

namespace Dumux
{
template <class TypeTag>
class CavitiesProblem;

namespace Properties
{
// Create new typetags
namespace TTag
{
struct RANSModel { using InheritsFrom = std::tuple<StaggeredFreeFlowModel>; };
struct CavitiesTypeTag { using InheritsFrom = std::tuple<RANSModel, KOmega>; };
} // end namespace TTag

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::CavitiesTypeTag>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
public:
    using type = FluidSystems::OnePLiquid<Scalar, Components::Constant<1, Scalar> >;
};

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::CavitiesTypeTag>
{
private:
    static constexpr auto dim = 2;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
public:
    using HostGrid = Dune::YaspGrid<dim, Dune::TensorProductCoordinates<Scalar, dim> >;
    using type = Dune::SubGrid<dim, HostGrid>;
};

// Set the problem properties
template<class TypeTag>
struct Problem<TypeTag, TTag::CavitiesTypeTag> { using type = Dumux::CavitiesProblem<TypeTag> ; };
template<class TypeTag>
struct EnableFVGridGeometryCache<TypeTag, TTag::CavitiesTypeTag> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::CavitiesTypeTag> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::CavitiesTypeTag> { static constexpr bool value = true; };
template<class TypeTag>
struct UpwindSchemeOrder<TypeTag, TTag::CavitiesTypeTag> { static constexpr int value = UPWINDSCHEMEORDER; };
} // end namespace Properties

/*!
 * \ingroup RANSTests
 * \brief  Test problem for the one-phase RANS problem in a  channel with cavities.
 */
template <class TypeTag>
class CavitiesProblem : public RANSProblem<TypeTag>
{
    using ParentType = RANSProblem<TypeTag>;

    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;

    using Element = typename FVGridGeometry::GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::FVGridGeometry>::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;

    using TimeLoopPtr = std::shared_ptr<CheckPointTimeLoop<Scalar>>;

    static constexpr auto dimWorld = FVGridGeometry::GridView::dimensionworld;

public:
    CavitiesProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry), eps_(1e-6)
    {
        inletVelocity_ = getParam<Scalar>("Problem.InletVelocity");
        Scalar kinematicViscosity = getParam<Scalar>("Component.LiquidKinematicViscosity");

        Dumux::TurbulenceProperties<Scalar, dimWorld, true> turbulenceProperties;
        Scalar diameter = this->fvGridGeometry().bBoxMax()[1] - this->fvGridGeometry().bBoxMin()[1];

        turbulentKineticEnergy_ = getParam<Scalar>("Problem.InletTurbulentKineticEnergy",
                                                   turbulenceProperties.turbulentKineticEnergy(inletVelocity_, diameter, kinematicViscosity));

        dissipation_ = getParam<Scalar>("Problem.InletDissipationRate",
                                        turbulenceProperties.dissipationRate(inletVelocity_, diameter, kinematicViscosity));

        stepHeight_ = getParam<Scalar>("Grid.StepHeight");
        Scalar stepDistance = getParam<Scalar>("Grid.StepDistance");
        Scalar stepLength = getParam<Scalar>("Grid.StepLength");

        stepCoords_[0][0] = getParam<Scalar>("Grid.StepBegin");
        stepCoords_[0][1] = stepCoords_[0][0] + stepLength;
        stepCoords_[1][0] = stepCoords_[0][1] + stepDistance;
        stepCoords_[1][1] = stepCoords_[1][0] + stepLength;

        std::cout << std::endl;
    }

   /*!
     * \name Problem parameters
     */
    // \{

    /*!
      * \brief Return the temperature within the domain in [K].
      *
      * This problem is isothermal but this functions is needed for compatibility.
      */
     Scalar temperature() const
     {
         return 283.15;
     }

   /*!
     * \brief Return the sources within the domain.
     *
     * \param globalPos The global position
     */
    NumEqVector sourceAtPos(const GlobalPosition &globalPos) const
    {
        return NumEqVector(0.0);
    }

    // \}
   /*!
     * \name Boundary conditions
     */
    // \{

   /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary control volume.
     *
     * \param globalPos The position of the center of the finite volume
     */
     BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
     {
         BoundaryTypes values;

         if (isOutlet_(globalPos))
         {
             values.setDirichlet(Indices::pressureIdx);

             values.setOutflow(Indices::turbulentKineticEnergyEqIdx);
             values.setOutflow(Indices::dissipationEqIdx);
         }
         else if (isOnUpperBoundary_(globalPos))
             values.setAllSymmetry();

         else// walls and inflow
         {
             values.setDirichlet(Indices::velocityXIdx);
             values.setDirichlet(Indices::velocityYIdx);

             values.setDirichlet(Indices::turbulentKineticEnergyIdx);
             values.setDirichlet(Indices::dissipationIdx);
         }
         return values;
     }

    /*!
     * \brief Returns whether a fixed Dirichlet value shall be used at a given cell.
     *
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param scv The sub control volume
     */
    template<class Element, class FVElementGeometry, class SubControlVolume>
    bool isDirichletCell(const Element& element,
                         const FVElementGeometry& fvGeometry,
                         const SubControlVolume& scv,
                         int pvIdx) const
    {
        // set a fixed dissipation (omega) for all cells at the wall
        for (const auto& scvf : scvfs(fvGeometry))
            if (isOnWallAtPos(scvf.center()) && pvIdx == Indices::dissipationIdx)
                return true;

        return false;
    }

     /*!
      * \brief Evaluate the boundary conditions for a dirichlet values at the boundary.
      *
      * \param element The finite element
      * \param scvf the sub control volume face
      * \note used for cell-centered discretization schemes
      */
    PrimaryVariables dirichlet(const Element &element, const SubControlVolumeFace &scvf) const
    {
        const auto globalPos = scvf.ipGlobal();
        PrimaryVariables values(initialAtPos(globalPos));

        return values;
    }

     /*!
      * \brief Evaluate the boundary conditions for fixed values at cell centers
      *
      * \param element The finite element
      * \param scv the sub control volume
      * \note used for cell-centered discretization schemes
      */
    PrimaryVariables dirichlet(const Element &element, const SubControlVolume &scv) const // set fixed cell chiama questa funzione
    {
        const auto globalPos = scv.center();
        PrimaryVariables values(initialAtPos(globalPos));

        unsigned int elementIdx = this->fvGridGeometry().elementMapper().index(element);
        const auto wallDistance = ParentType::wallDistance_[elementIdx];
        values[Indices::dissipationEqIdx] = 6.0 * ParentType::kinematicViscosity_[elementIdx]
                                            / (ParentType::betaOmega() * wallDistance * wallDistance);

        return values;
    }

    // \}

   /*!
     * \name Volume terms
     */
    // \{

    /*!
      * \brief Evaluate the initial value for a control volume.
      *
      * \param globalPos The global position
      */
     PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
     {
         PrimaryVariables values(0.0);
         values[Indices::pressureIdx] = 1.1e+5;

         if (globalPos[1] > stepHeight_ + eps_)
            values[Indices::velocityXIdx] = inletVelocity_;

        values[Indices::turbulentKineticEnergyIdx] = turbulentKineticEnergy_;
        values[Indices::dissipationIdx] = dissipation_;

         if (isOnWallAtPos(globalPos))
         {
             values[Indices::velocityXIdx] = 0.0;
             values[Indices::turbulentKineticEnergyIdx] = 0.0;
             values[Indices::dissipationIdx] = 0.0;
         }

         return values;
     }

    // \}

    void setTimeLoop(TimeLoopPtr timeLoop)
    {
        timeLoop_ = timeLoop;
    }

    Scalar time() const
    {
        return timeLoop_->time();
    }

    bool isOnWallAtPos(const GlobalPosition& globalPos) const
    {
        return isOnLowerBoundary_(globalPos);
    }

private:

    bool isInlet_(const GlobalPosition& globalPos) const
    {
        return globalPos[0] < this->fvGridGeometry().bBoxMin()[0] + eps_;
    }

    bool isOutlet_(const GlobalPosition& globalPos) const
    {
        return globalPos[0] > this->fvGridGeometry().bBoxMax()[0] - eps_;
    }

    bool isOnUpperBoundary_(const GlobalPosition& globalPos) const
    {
        return globalPos[1] > this->fvGridGeometry().bBoxMax()[1] - eps_;
    }

    bool isOnLowerBoundary_(const GlobalPosition& globalPos) const
    {
        if (globalPos[1] > this->fvGridGeometry().bBoxMin()[1] + stepHeight_ + eps_)
            return false;

        if (globalPos[1] > this->fvGridGeometry().bBoxMin()[1] + eps_ && isInCavities_(globalPos[0]))
            return false;
        return true;
    }

    bool isInCavities_(const Scalar& x) const
    {
        if ((x > stepCoords_[0][0] && x < stepCoords_[0][1]) || (x > stepCoords_[1][0] && x < stepCoords_[1][1]))
            return true;
        else
            return false;
    }

    Scalar eps_;
    Scalar inletVelocity_;
    Scalar inletTemperature_;
    Scalar wallTemperature_;
    Scalar turbulentKineticEnergy_;
    Scalar dissipation_;
    Scalar stepHeight_;
    std::array<std::array<Scalar, 2>, 2> stepCoords_;
    TimeLoopPtr timeLoop_;
};

} // end namespace Dumux

#endif // DUMUX_CAVITIES_PROBLEM_HH
