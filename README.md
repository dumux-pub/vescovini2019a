# The Project

**Author :** Andrea Vescovini

This DuMuX module contains the code for producing the results of the Master's thesis:

**_A fully implicit higher order staggered grid formulation of the Navier-Stokes equations for coupled flow systems_**

## Installation

The easiest way to install this module is to run the script [installVescovini2019a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/vescovini2019a/raw/master/installVescovini2019a.sh) contained in this folder, typing

```shell
mkdir -p Vescovini2019a && cd Vescovini2019a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/vescovini2019a/raw/master/installVescovini2019a.sh
chmod u+x installVescovini2019a.sh
./installVescovini2019a.sh
```

This will download and install DuMuX and all the required DUNE modules. For a successful installation the following software packages are required:
* git
* CMake 3.1 or newer.
* pkg-config
* A C++ compiler (GCC 4.9 or newer and Clang 3.8 or newer are supported).
* A C compiler
* A Fortran compiler

Moreover the following libraries are required by the applications of this module:
* Suitesparse

For more detailed information have a look at the [DuMuX installation guide](https://dumux.org/installation) or at the [DuMuX handbook](https://dumux.org/documents/dumux-handbook-3.0.pdf).

## Applications

The source code of the applications can be found in the subfolders of the folder `appl`:
* `1d` contains the code for the one-dimensional steady Navier-Stokes problem used to test the convergence.
* `angeli` contains the code for the two-dimensional unsteady Navier-Stokes problem used to test the convergence.
* `bacwardfacingstep` constains the code for the two-dimensional unsteady RANS problem used to to test the _k-ω_ turbulence model.
* `cavities` contains the code for the two-dimensional unsteady RANS problem with two cavities on the lower boundary.
* `cavitiescoupled` contains the code for the two-dimensional unsteady coupled problem between a free-flow and a porous-medium flow.
* `kovasznay` contains the code for the two-dimensional steady Navier-Stokes problem used to test the convergence.
* `obstaclecoupled` contains the code for the two-dimensional unsteady coupled problem between a free-flow and a porous-medium obstacle.
* `roughchannel` contains the code for the two-dimensional unsteady Navier-Stokes problem with a non-flat lower boundary.
* `roughchannelturbulent` contains the code for the two-dimensional unsteady RANS problem with a non-flat lower boundary.
* `sincos` contains the code for the two-dimensional steady Navier-Stokes problem used to test the convergence.
* `sincosunsteady` contains the code for the two-dimensional unsteady Navier-Stokes problem used to test the convergence.
* `steadychannel` contains the code for the two-dimensional steady Navier-Stokes problem used to test the convergence.

Inside the folder `appl`, the script `convergence.sh` can be used to perform spatial convergence tests.

## Run Simulations

After the installation of this module you will find the folder `vescovini2019a/build-cmake`. You can build and run the applications going into this directory and for example typing:
```shell
cd appl/1d
make 1d_navierstokes
./1d_navierstokes
```
The executable will print information about the simulation and will produce `*.vtp` or `*.vtu` files that can be visulized using a visualization software, e.g. Paraview.
